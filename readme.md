Exploring OWIN Nancy Framework
==============================

# OWIN
> Open Web Interface for .NET. OWIN is a specification for defining interfaces between .net applications and web server. You can find more at [OWIN] (http://owin.org)

# Nancy Framework
> A light-weight, oepn source, low ceremony web framework for building http based services. You can find more at [Nancy Fx] (http://http://nancyfx.org/)

# Goal

	Goal here is to explore owin, nancy framework and self hosting option. 
	For this we will be using some components from Katana project and middleware provided by nancy framework. 
	So ride along and see for yourself what all this fuss is about. Cheers !